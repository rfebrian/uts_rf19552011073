import 'package:flutter/material.dart';
import 'package:uts_rf19552011073/models/product.dart';

class DetailProduct extends StatelessWidget {
  const DetailProduct({Key? key, required this.product}) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: PreferredSize(
      preferredSize: Size.fromHeight(60),
      child: AppBar(
        actions: [
          Container(
            padding: EdgeInsets.all(5),
            width: MediaQuery.of(context).size.width / 1,
            height: MediaQuery.of(context).size.height /1,
            color: Colors.white70,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, border: Border.all(color: Color(int.parse(product.color)))),
                  child: IconButton(
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onPressed: (){
                        Navigator.pop(context);
                      }, icon: Icon(Icons.arrow_back_ios_new, color: Color(int.parse(product.color)),)),
                  alignment: Alignment.center,
                ),
                Image.asset('assets/images/fujifilm-banner.png',
                    width: MediaQuery.of(context).size.width / 3),
                Container(
                  decoration: BoxDecoration(shape: BoxShape.circle,
                    color: Colors.black,),
                    child: IconButton(
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onPressed: (){
                        }, icon: Icon(Icons.more_vert_rounded, color: Colors.white,)))

              ],
            ),
          ),
        ],
        backgroundColor: Colors.white,
        elevation: 1,
        centerTitle: true,
      ),
    ),
    body: Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Center(
          child: Container(
            height: MediaQuery.of(context).size.height / 3,
            width: MediaQuery.of(context).size.width / 2,
                child: Image.asset(product.url)),
        ),
        Container(
          padding: EdgeInsets.only(top: 15, left: 15, right: 15),
          height: MediaQuery.of(context).size.height / 2.4,
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  text: TextSpan(
                      children: [
                        TextSpan(text: 'Instax ', style: TextStyle(color: Colors.black, fontSize: 15, fontWeight: FontWeight.bold)),
                        TextSpan(text: product.name, style: TextStyle(fontSize: 17,fontWeight: FontWeight.bold,color: Color(int.parse(product.color))))
                      ]
                  ),
                ),
                SizedBox(height: 15),
                Text("Be real and fun with the INSTAX MINI 7+. Cool design, colorful and compact, this instant camera is fun andeasy to use.Point and shoot and give your day some fun!"),
                SizedBox(height: 15),
                Text("Point & Shoot", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
                SizedBox(height: 15),
                Text("The Mini 7+ is easy to use! Simply point and shoot! With its exposure control adjustment and 60mm fixed-focuslens, the Mini 7+ makes it easy for you to be creative and live in the moment."),
                SizedBox(height: 15),
                Text("Mini But With Full-Size Memories", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
                SizedBox(height: 15),
                Text("Pop it in your wallet, stick it to your wall – the INSTAX Mini film brings you instant 2 x 3 sized photos youcan show and tell."),
                SizedBox(height: 15),
                Text("Using professional high-quality film technology (as you’d expect from Fujifilm), your festival frolicking, sunworshipping, crowd surfing memories that you print will transport you right back into that moment."),
                SizedBox(height: 15),
                Text("Mini Film", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
                SizedBox(height: 15),
                Text("Mini moments with maximum impact. What’s your next mini moment?"),
                SizedBox(height: 15),
                Text("Plenty of Great Color Choices", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
                SizedBox(height: 15),
                Text("Available in five awesome colors: Lavender, Seafoam Green, Coral, Light Pink & Light Blue"),
                SizedBox(height: 15),
                Text("The Mini 7+ Has Your Back!", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
                SizedBox(height: 15),
                Text("Depending upon the weather conditions, you can easily control brightness to obtain a great picture"),
                SizedBox(height: 15),
                Text("Fun All The Time!", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
                SizedBox(height: 15),
                Text("Live in the moment and enjoy your Mini 7+, and give your day some instant fun!"),
                SizedBox(height: 15),
              ],
            ),
          ),
        )
      ],
    ),
    bottomNavigationBar: BottomAppBar(
      child: Container(
        padding: EdgeInsets.all(15),
        width: MediaQuery.of(context).size.width / 1,
        height: MediaQuery.of(context).size.height / 11,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              child: Text(product.price, style: TextStyle(fontSize: 27, fontWeight: FontWeight.bold),),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 3.5,
              alignment: Alignment.center,
              decoration: BoxDecoration(shape: BoxShape.rectangle, borderRadius: BorderRadius.circular(15), color: Color(int.parse(product.color))),
              child: Text("Buy Now", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
            )
          ],
        ),
      ),
    ),
  );
  }