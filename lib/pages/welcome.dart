import 'package:flutter/material.dart';
import 'dart:async';
import 'home.dart';

class Welcome extends StatefulWidget {
  @override
  _WelcomeState createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> {
  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 4),
        () => Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => Home())));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          height: MediaQuery.of(context).size.height / 1,
          width: MediaQuery.of(context).size.width / 1,
          color: Colors.white,
          child: Stack(children: [
            Center(
              child: Container(
                  color: Colors.white,
                  child: Container(
                    child: Image.asset(
                      'assets/images/fujifilm.png',
                      width: MediaQuery.of(context).size.width / 1.5,
                    ),
                  )),
            ),
            Container(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).size.height / 30),
              alignment: Alignment.bottomCenter,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RichText(
                      text: TextSpan(children: [
                    TextSpan(
                        text: "Made With ",
                        style: TextStyle(color: Colors.black, fontSize: 12)),
                    WidgetSpan(
                        child: Icon(
                      Icons.favorite,
                      size: 15,
                      color: Colors.pink,
                    )),
                    TextSpan(
                        text: " | © 2021 ",
                        style: TextStyle(fontSize: 12, color: Colors.black))
                  ]))
                ],
              ),
            )
          ]),
        ),
      ),
    );
  }
}
