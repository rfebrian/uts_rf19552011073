import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uts_rf19552011073/models/product.dart';
import 'package:uts_rf19552011073/datas/product_data.dart';
import 'package:uts_rf19552011073/pages/detail_product.dart';
import 'package:uts_rf19552011073/widgets/search.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Product> products = allProducts;
  String query = '';

  @override
  void iniState() {
    super.initState();

    products = allProducts;
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: AppBar(
            actions: [
              Container(
                padding: EdgeInsets.all(5),
                width: MediaQuery.of(context).size.width / 1,
                height: MediaQuery.of(context).size.height /1,
                color: Colors.white70,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: Colors.deepOrange),
                      child: IconButton(
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          onPressed: (){
                            Navigator.pop(context);
                          }, icon: Icon(Icons.menu, color: Colors.white,)),
                      alignment: Alignment.center,
                    ),
                    Image.asset('assets/images/fujifilm-banner.png',
                        width: MediaQuery.of(context).size.width / 3),
                    Container(
                        decoration: BoxDecoration(shape: BoxShape.circle,
                          color: Colors.black,),
                        child: IconButton(
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            onPressed: (){
                            }, icon: Icon(Icons.shopping_bag_outlined, color: Colors.white,)))

                  ],
                ),
              ),
            ],
            backgroundColor: Colors.white,
            elevation: 1,
            centerTitle: true,
          ),
        ),
        body: Column(
          children: <Widget>[
            buildSearch(),
            Expanded(
              child: ListView.builder(
                itemCount: products.length,
                itemBuilder: (context, index) {
                  final product = products[index];

                  return buildProduct(product);
                },
              ),
            ),
          ],
        ),
      );

  Widget buildSearch() => SearchWidget(
        text: query,
        hintText: 'Search',
        onChanged: searchProduct,
      );

  Widget buildProduct(Product product) => Stack(
    children: [
      Center(
        child: Container(
          width: MediaQuery.of(context).size.width / 1.1,
          height: MediaQuery.of(context).size.height / 4.5,
          decoration: BoxDecoration(shape: BoxShape.rectangle,borderRadius: BorderRadius.circular(15)),
          child: Stack(
            children: [
              Container(
              width: MediaQuery.of(context).size.width / 1.15,
              height: MediaQuery.of(context).size.height / 5,
              decoration: BoxDecoration(shape: BoxShape.rectangle, color: Color(int.parse(product.color)),borderRadius: BorderRadius.circular(15)),
            ),
              Container(
                padding: EdgeInsets.only(top: 15, left: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                  Text(product.title, style: TextStyle(fontSize: 11,color: Colors.white)),
                  SizedBox(height: 10,),
                  Row(
                    children: [
                      Text("Instax", style: TextStyle(color: Colors.white)),
                      SizedBox(width: MediaQuery.of(context).size.width/100,),
                      Text(product.name, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),)
                    ],
                  ),
                    SizedBox(height: 10,),
                    Text(product.price, style: TextStyle(fontSize: 25,color: Colors.white, fontWeight: FontWeight.bold)),
                    SizedBox(height: 10,),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20))),
                      child: Text('Buy', style: TextStyle(fontSize: 15, color: Color(int.parse(product.color))),),
                      onPressed: () {
                        Navigator.push(
                          context, MaterialPageRoute(builder: (context) => DetailProduct(product: product))
                        );
                      },
                    )
                ],),
              ),
            ]
          ),
        ),
      ),
      Container(
        padding: EdgeInsets.only(right: 15, top: 20),
        alignment: Alignment.centerRight,
        child: Image.asset(
          product.url, width: MediaQuery.of(context).size.width/3.2,
        ),
      ),
    ]
  );

  void searchProduct(String query) {
    final products = allProducts.where((product) {
      final titleLower = product.title.toLowerCase();
      final nameLower = product.name.toLowerCase();
      final searchLower = query.toLowerCase();

      return titleLower.contains(searchLower) ||
          nameLower.contains(searchLower);
    }).toList();

    setState(() {
      this.query = query;
      this.products = products;
    });
  }
}