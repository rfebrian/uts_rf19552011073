class Product {
  final int id;
  final String title;
  final String name;
  final String price;
  final String url;
  final String color;

  const Product(
      {required this.id,
      required this.title,
      required this.name,
      required this.price,
      required this.url,
      required this.color});

  Map<String, dynamic> toJson() =>
      {'id': id, 'title': title, 'name': name, 'price': price, 'url': url, 'color': color};
}
