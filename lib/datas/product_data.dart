import 'package:uts_rf19552011073/models/product.dart';

final allProducts = <Product>[
  Product(
      id: 1,
      title: 'Limited Edition',
      name: 'Mini Mint 7+',
      price: '\$ 49.90',
      url: 'assets/images/InstaxMini7plus_Mint_R.png',
      color: "0xFF70b1a1"),
  Product(
      id: 2,
      title: 'Limited Edition',
      name: 'Mini Blue 7+',
      price: '\$ 50.90',
      url: 'assets/images/InstaxMini7plus_Blue_R.png',
      color: "0xFF77a0c6"),
  Product(
      id: 3,
      title: 'Limited Edition',
      name: 'Mini Choral 7+',
      price: '\$ 51.90',
      url: 'assets/images/InstaxMini7plus_Choral_R.png',
      color: "0xFFb0463c"),
  Product(
      id: 4,
      title: 'Limited Edition',
      name: 'Mini Pink 7+',
      price: '\$ 53.90',
      url: 'assets/images/InstaxMini7plus_Pink_R.png',
      color: "0xFFfcf9496"),
  Product(
      id: 5,
      title: 'Limited Edition',
      name: 'Mini Lavender 7+',
      price: '\$ 52.90',
      url: 'assets/images/InstaxMini7plus_Lavender_R.png',
      color: "0xFF855f8c"),

];
