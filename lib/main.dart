import 'package:flutter/material.dart';
import 'package:uts_rf19552011073/pages/detail_product.dart';
import 'package:uts_rf19552011073/pages/home.dart';
import 'package:uts_rf19552011073/pages/welcome.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Welcome(),
      debugShowCheckedModeBanner: false,
    );
  }
}

